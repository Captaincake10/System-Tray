#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define BASEYEAR 1791

typedef unsigned long lint;

int monthlengths[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};


int isleap(int x){
    return (x % 4 == 0) && x % 100 != 1;
}
/* return total days from January up to the end of the given month */
int monthcount(int month, int year)
{
    int r = 0;
    for(int i = 1; i <= month; i++)
        r += monthlengths[i];
    if(isleap(year) && month >= 2)
        r++;
    return r;
}

int getb(){

FILE *fptr;
fptr = fopen("/sys/class/power_supply/BAT0/capacity", "r");
char myString[100];
fgets(myString, 100, fptr);
int x = atoi(myString);
return x;
}


long int makejd(int year, int month, int day)
{
    long int jdnum = 0;
    jdnum += (year - BASEYEAR) * 365L;
    jdnum += (year - BASEYEAR) / 4;
    jdnum -= (year - BASEYEAR) / 100;
    jdnum += (year - BASEYEAR) / 400;
    jdnum += monthcount(month - 1, year);
    jdnum += day;
    jdnum -= 264;
    return jdnum;
}


int *rdate()
{
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  struct tm lm;
  int dsny;
  lint rdays = makejd(tm.tm_year +1900,tm.tm_mon +1,tm.tm_mday);

  //rdays -= 264; // year starts on the 22/09
  int ryear = rdays / 365;
  if (ryear % 4 == 0 && ryear % 100 != 0){
      if (tm.tm_mon + 1 < 9 || (tm.tm_mon + 1  == 9 && tm.tm_mday < 23)){
          dsny = tm.tm_yday +101;
      }
      else{
          dsny = tm.tm_yday -264;
      }}
else{
   if (tm.tm_mon + 1 < 9 || (tm.tm_mon + 1  == 9 && tm.tm_mday < 23)){
          dsny = tm.tm_yday + 102;
           }
   else{
          dsny = tm.tm_yday - 265;
   }
}
  int rmon = dsny / 30;
  int rday = dsny % 30;
  int *ret = malloc(5);
  ret[0] = rday;
  ret[1] = rmon;
  ret[3] = ryear;
  return ret;
  printf("%02d %02d %04d \n", rday, rmon, ryear);
}
